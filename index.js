const TelegramBot = require('node-telegram-bot-api');
const cron = require('node-cron');
const express = require('express');

require('dotenv').config();

const app = express();

const { CHAT_ID, LOVE_MESSAGES } = require('./constants');

console.log('Bot has been started...', CHAT_ID);

const bot = new TelegramBot(process.env.BOT_TOKEN, {
  polling: true,
});

const sendMessage = async ({ chatId = CHAT_ID, text, options = {} }) => {
  const sent = await bot.sendMessage(chatId, text, {
    parse_mode: 'Markdown',
    ...options,
  });

  return sent;
};

const usedMessages = new Map();

const pickRandomMessage = (type = null) => {
  let messagesToPick = [];
  if (type && LOVE_MESSAGES.has(type)) {
    messagesToPick = LOVE_MESSAGES.get(type);
  } else {
    const messages = [...LOVE_MESSAGES.values()].flat();
    messagesToPick = messages;
  }

  return {
    message: messagesToPick[Math.floor(Math.random() * messagesToPick.length)],
    messagesCount: messagesToPick.length,
  };
};

const pickAndSendMessage = (type = null) => {
  const { message, messagesCount } = pickRandomMessage(type);

  if (usedMessages.has(type) && usedMessages.get(type).includes(message)) {
    if (usedMessages.get(type).length === messagesCount) {
      usedMessages.delete(type);
    }
    return pickAndSendMessage(type);
  }

  usedMessages.set(type, message);

  sendMessage({ text: message });
};

cron.schedule('00 07 * * *', () => pickAndSendMessage('morning'));
cron.schedule('00 11 * * *', () => pickAndSendMessage('common'));
cron.schedule('00 17 * * *', () => pickAndSendMessage('sexy'));
cron.schedule('00 21 * * *', () => pickAndSendMessage('night'));

app.get(`/`, function (req, res) {
  res.send('Love Messages Telegram bot');
});

app.post(`/${process.env.BOT_TOKEN}`, function (req, res) {
  console.log('Webhook body ', req.body);
  res.send(true);
});

bot.onText(/ранкове/, () => pickAndSendMessage('morning'));
bot.onText(/нічне/, () => pickAndSendMessage('night'));
bot.onText(/сексі/, () => pickAndSendMessage('sexy'));
bot.onText(/шось/, () => pickAndSendMessage('common'));

bot.setWebHook(
  `https://love-messages-bot.herokuapp.com/${process.env.BOT_TOKEN}`,
);

const PORT = process.env.PORT || 4444;

app.listen(PORT, () => console.log(`Express is listening on port ${PORT}`));
